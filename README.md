# lathund

## En lathund på svenska för vim

### Förflytta sig i texten

- Genom att trycka `w` förflyttar du dig ett ord (word) framåt i texten. trycker du `W` räknas 'ord' som alla tecken mellan två whitespaces. 
- Genom att trycka `b` förflyttar du dig ett ord bakåt (back) i texten. trycker du `B` räknas 'ord', precis som i fallet med `W` som alla tecken mellan två whitespaces. 
- Genom att trycka `e` förflyttar du dig till slutet av ordet. trycker du `E` räknas 'ord' som allt mellan whitespaces. 
- Skriv en siffra före `w`, `b` eller `e` (eller motsvarande versaler) så förflyttar du dig detta antal ord. `5w` förflyttar dig alltså fem ord framåt.

### Förflytta sig på sidan

- Använd `H` för att flytta markören högst upp på sidan (high).
- Använd `M` för att flytta markören till mitten av sidan (middle).
- Använd `H` för att flytta markören längst ned på sidan (low).
- `Ctrl` - `u` scrollar en halv sida uppåt (up_) 
- `Ctrl` - `d` scrollar en halv sida nedåt (down) 

### Söka i texten

- för att hitta fler förekomster av ett ord i texten, ställ markören på ordet och tryck `*` för att hitta nästa förekomst eller `#` för att hitta föregående.

### Ändra text

- `cw` tar bort resten av ordet och ändrar till insert-läge
- `caw` tar bort hela ordet (alltså även om du står i mitten av ordet) och ändrar till insert-läge. 
- `cW` resp `caW` räknar 'ord' som allt som finns mellan whitespaces

### Öppna ny fil

- `Ex` (Explore) öppnar filträdet på det ställe som den senaste filen/bufferten "är" i.


### Kopiera till/från datorns urklippsminne

#### Kopiera till urklippsminnet
- markera texten med `v` eller `V`
- tryck `"+y` där `"+` är vim's minnesbank (`"`)  som är kopplad till ditt urlklippsminne (`+`) och `y` är för att kopiera

#### Klistra in från urklippsminnet
- tryck `"+p` där `"+` är vim's minnesbank (`"`)  som är kopplad till ditt urlklippsminne (`+`) och `p` är för att klistra in

## En lathund på svenska för MySql

### Skapa en MySql-användare med lösenord.

- Skapa först användaren med lösenord:  
`mysql> CREATE USER 'ohdear_ci'@'localhost' IDENTIFIED BY 'ohdear_secret';`

- Sedan ger du den användaren rättigheter till databasen:  
`mysql> GRANT ALL ON ohdear_ci.* TO 'ohdear_ci'@'localhost';`

